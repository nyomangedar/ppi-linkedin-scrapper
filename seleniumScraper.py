from unittest import expectedFailure
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
import pandas as pd



s = Service("D:\\UniversityStuff\SelfExercise\chromedriver")
driver = webdriver.Chrome()
contactLinkList = {}
uobLink = "https://www.linkedin.com/search/results/people/?geoUrn=%5B%22102478259%22%5D&origin=FACETED_SEARCH&page={page}&schoolFilter=%5B%22166627%22%5D&sid=J27"
bcuLink = "https://www.linkedin.com/search/results/people/?geoUrn=%5B%22102478259%22%5D&origin=FACETED_SEARCH&page={page}&schoolFilter=%5B%2218068%22%5D&sid=_S)"
ucb = "https://www.linkedin.com/search/results/people/?geoUrn=%5B%22102478259%22%5D&origin=GLOBAL_SEARCH_HEADER&page={page}&schoolFilter=%5B%221270848%22%5D&sid=hxX"
aston= "https://www.linkedin.com/search/results/people/?geoUrn=%5B%22102478259%22%5D&origin=GLOBAL_SEARCH_HEADER&page={page}&schoolFilter=%5B%2216267%22%5D&sid=s5)"

numberOfPages = [3,100, 16, 13]

searchLink = [ ucb,uobLink, bcuLink, aston]
uniName = ['UCB','UoB','BCU', 'Aston']

print('email')
email = input()
print('password')
password = input()


def main():
    
    
    driver.get('https://www.linkedin.com/')
    driver.implicitly_wait(5)

    emailInput = driver.find_element(By.XPATH, '//*[@id="session_key"]')
    emailInput.send_keys(email)

    passwordInput = driver.find_element(By.XPATH, '//*[@id="session_password"]')
    passwordInput.send_keys(password)

    signIn = driver.find_element(By.XPATH, '//*[@id="main-content"]/section[1]/div/div/form/button')
    signIn.click()
    # accountScrap("https://www.linkedin.com/in/alma-mahira-lazuardani/")

    for i in range(len(searchLink)):
        contactLinkList={}
        miss = searchScrap(searchLink[i], numberOfPages[i], contactLinkList)
        print("missing contact: ", miss)
        df = pd.DataFrame(contactLinkList)
        print(df)
        df.to_csv('{uniName}.csv'.format(uniName=uniName[i]))
    
    print("finish")

def searchScrap(searchLink, pageNumber, list):
    missingContact = 0
    for j in range(2,pageNumber+1):
        print(pageNumber, "pages")
        page = searchLink.format(page=j)
        driver.get(page)
        driver.implicitly_wait(3)
        # contactCandidate = []
        for i in range(1,11):
            # Check display name
            try:
                contactNama = driver.find_element(By.XPATH, '//*[@id="main"]/div/div/div[2]/ul/li[{number}]/div/div/div[2]/div[1]/div[1]/div/span[1]/span/a/span/span[1]'.format(number=i))
            except NoSuchElementException:
                try:
                    contactNama = driver.find_element(By.XPATH, '//*[@id="main"]/div/div/div[1]/ul/li[{number}]/div/div/div[2]/div[1]/div[1]/div/span[1]/span/a/span/span[1]'.format(number=i))
                except NoSuchElementException:
                    missingContact += 1
                    print(missingContact)
                    continue
            nama = contactNama.get_attribute('innerHTML')
            nama = nama.replace("<!---->", "")
            # grab link
            try:
                contactLink = driver.find_element(By.XPATH, '//*[@id="main"]/div/div/div[2]/ul/li[{number}]/div/div/div[2]/div[1]/div[1]/div/span[1]/span/a'.format(number=i))
            except NoSuchElementException:
                contactLink = driver.find_element(By.XPATH, '//*[@id="main"]/div/div/div[1]/ul/li[{number}]/div/div/div[2]/div[1]/div[1]/div/span[1]/span/a'.format(number=i))
            link = contactLink.get_attribute('href')
            
            link = link.partition('?')[0]
            list[nama] = [link]
            print(list)
            # contactCandidate.append(nama)
            print("Search iteration: ", i)

        # for i in contactCandidate:
        #     accountScrap(i,contactLinkList[i][0])
        # print("current page: ", j)
        # print(contactLinkList)
    return missingContact
            
    

def accountScrap(name, link):
    driver.get(link)
    driver.implicitly_wait(4)
    educationList = driver.find_element(By.ID, 'education')
    educationList = educationList.find_element(By.XPATH, './..')
    emberId = educationList.get_attribute('id')
    educationList = educationList.find_elements(By.XPATH, '//*[@id="{emberId}"]/div[3]/ul/li'.format(emberId=emberId))
    for i in range(1,len(educationList)+1):
        institutionName = "null"
        degreeName = "null"
        graduationYear = "null"
        institutionElem = driver.find_element(By.XPATH, '//*[@id="{emberId}"]/div[3]/ul/li[{number}]/div/div[2]/div[1]/a/div/span/span[1]'.format(number=i, emberId=emberId))
        institutionName = institutionElem.get_attribute('innerHTML')
        institutionName = institutionName.replace("<!---->", "")
        if institutionName in acceptedUniversity:
            print(institutionName)
            degreeElem = driver.find_element(By.XPATH, '//*[@id="{emberId}"]/div[3]/ul/li[{number}]/div/div[2]/div[1]/a/span[1]/span[1]'.format(number=i, emberId=emberId))
            degreeName = degreeElem.get_attribute('innerHTML')
            degreeName = degreeName.replace("<!---->", "")
            try:
                graduationYear = driver.find_element(By.XPATH, '//*[@id="{emberId}"]/div[3]/ul/li[{number}]/div/div[2]/div[1]/a/span[2]/span[1]'.format(number=i, emberId=emberId))
                graduationYear = graduationYear.get_attribute('innerHTML')
                graduationYear = graduationYear.replace("<!---->", "")
            except NoSuchElementException:
                pass
            break
    contactLinkList[name].append(name)
    contactLinkList[name].append(institutionName)
    contactLinkList[name].append(degreeName)
    contactLinkList[name].append(graduationYear)
    return


if __name__ == "__main__":
    main()


    # //*[@id="main"]/div/div/div[1]/ul/li[1]/div/div/div[2]/div[1]/div[1]/div/span/span/a
    # //*[@id="main"]/div/div/div[1]/ul/li[{number}]/div/div/div[2]/div[1]/div[1]/div/span[1]/span/a/span/span[1]

    # //*[@id="ember399"]/div[3]/ul/li[3]/div/div[2]/div[1]/a/span[2]/span[1]
    # //*[@id="{emberId}"]/div[3]/ul/li[{number}]/div/div[2]/div[1]/a/span[2]/span[1]
    # //*[@id="ember399"]/div[3]/ul/li[2]/div/div[2]/div[1]/a/span[2]/span[1]
    # //*[@id="ember105"]/div[3]/ul/li[1]/div/div[2]/div/a/span[2]/span[1]
    # //*[@id="ember105"]/div[3]/ul/li[2]/div/div[2]/div[1]/a/span[2]/span[1]
    # https://www.linkedin.com/search/results/people/?geoUrn=%5B%22102478259%22%5D&keywords=university%20of%20birmingham&origin=FACETED_SEARCH&schoolFilter=%5B%221270848%22%2C%2216267%22%2C%22166627%22%2C%2218068%22%5D&sid=-tK
    # 'https://www.linkedin.com/search/results/people/?geoUrn=%5B%22102478259%22%5D&keywords=university%20of%20birmingham&origin=FACETED_SEARCH&page={number}&schoolFilter=%5B%221270848%22%2C%2216267%22%2C%22166627%22%2C%2218068%22%5D&sid=VJv'.format(page=j)

    # '//*[@id="main"]/div/div/div[2]/ul/li[{number}]/div/div/div[2]/div[1]/div[1]/div/span[1]/span/a/span/span[1]'
    # '//*[@id="main"]/div/div/div[2]/ul/li[{number}]/div/div/div[2]/div[1]/div[1]/div/span[1]/span/a/span/span[1]'.fomat(number=i)
    # '//*[@id="main"]/div/div/div[2]/ul/li[{number}]/div/div/div[2]/div[1]/div[1]/div/span[1]/span/a'.format(number=i)
    # '//*[@id="main"]/div/div/div[1]/ul/li[{number}]/div/div/div[2]/div[1]/div[1]/div/span[1]/span/a'
    # '//*[@id="main"]/div/div/div[2]/ul/li[{number}]/div/div/div[2]/div[1]/div[1]/div/span[1]/span/a'.format(number=i)
    # '//*[@id="main"]/div/div/div[1]/ul/li[{number}]/div/div/div[2]/div[1]/div[1]/div/span[1]/span/a'.format(number=i))
    #education
    # //*[@id="ember105"]/div[3]/ul/li[1]/div/div[2]/div[1]/a/div/span/span[1]
    # //*[@id="ember105"]/div[3]/ul/li[4]/div/div[2]/div[1]/a/div/span/span[1]
    # //*[@id="ember70"]/div[3]/ul
    # //*[@id="ember32"]/div[2]/div[2]/div[1]/div[1]/h1
    # //*[@id="ember104"]/div[3]/ul/li[1]/div/div[2]/div[1]/a/div/span/span[1]
    # //*[@id="ember3837"]/div[3]/ul/li[1]/div/div[2]/div[1]/a/div/span/span[1]
    # //*[@id="ember3837"]/div[3]/ul/li[1]/div/div[2]/div[1]/a/span[1]/span[1]

    # //*[@id="ember3837"]/div[3]/ul/li[2]/div/div[2]/div[1]/a/div/span/span[1]
    # //*[@id="ember3837"]/div[3]/ul/li[2]/div/div[2]/div[1]/a/span[1]/span[1]



# driver.get('https://www.linkedin.com/search/results/people/?geoUrn=%5B%22102478259%22%5D&keywords=university%20of%20birmingham&origin=FACETED_SEARCH&sid=Vui')

# directToSignIn = driver.find_element(By.XPATH, '/html/body/div[2]/main/p[1]/a')
# directToSignIn.click()

# emailInput = driver.find_element(By.XPATH, '//*[@id="username"]')
# emailInput.send_keys('nyoman.gedar@gmail.com')

# password = driver.find_element(By.XPATH, '//*[@id="password"]')
# password.send_keys('eze485ih78')

# signIn = driver.find_element(By.XPATH, '//*[@id="organic-div"]/form/div[3]/button')
# signIn.click()

# ulResult = driver.find_element(By.XPATH, '//*[@id="main"]/div/div/div[1]/ul')
# print(ulResult)